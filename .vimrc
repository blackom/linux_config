"set nocompatible

"filetype off
"source $VIMRUNTIME/defaults.vim
let g:skip_defaults_vim = 1
syntax enable
syntax reset


set rtp+=~/.vim/bundle/Vundle.vim

call vundle#begin()

Plugin 'VundleVim/Vundle.vim'
Plugin 'preservim/nerdtree'
Plugin 'tpope/vim-fugitive'
Plugin 'flazz/vim-colorschemes'
Plugin 'Preservim/tagbar'
Plugin 'vim-scripts/OmniCppComplete'
"Plugin 'ycm-core/YouCompleteMe'
"Plugin 'bfrg/vim-cpp-modern'
Plugin 'octol/vim-cpp-enhanced-highlight'

call vundle#end()
filetype plugin indent on

filetype plugin on
"au FileType c setl ofu=complete#CompleteCpp


set wrap

set textwidth=80
set tabstop=4
set softtabstop=4
set shiftwidth=4
set expandtab
set noshiftround

set number
"set modelines=0
set modeline

set encoding=utf8

set cursorline

set background=dark

colorscheme "visualstudio"
set showmatch
set matchtime=3

map <C-n> :NERDTreeToggle<CR>
map <F8> :TagbarToggle<CR>
